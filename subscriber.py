import time
import os
import random
import sys
from google.cloud import pubsub_v1
from google.api_core.exceptions import AlreadyExists

#
# Docs
# https://googleapis.dev/python/pubsub/latest/index.html
# https://cloud.google.com/container-registry/docs/pub-sub-notifications
#

project_id = os.environ.get('PROJECT_ID', None)
topic_name = os.environ.get('TOPIC_NAME', 'container-analysis-occurrences-v1')
subscription_name = os.environ.get('SUBSCRIPTION_NAME')

subscriber = pubsub_v1.SubscriberClient.from_service_account_file(os.environ.get('GOOGLE_APPLICATION_CREDENTIALS'))

# in the form `projects/{project_id}/topics/{topic_name}`
topic_path = 'projects/{}/topics/{}'.format(project_id, topic_name)

# The `subscription_path` method creates a fully qualified identifier
# in the form `projects/{project_id}/subscriptions/{subscription_name}`
subscription_path = subscriber.subscription_path(
    project_id, subscription_name)


print("GOOGLE_APPLICATION_CREDENTIALS: {}".format(os.environ.get('GOOGLE_APPLICATION_CREDENTIALS')))
print("Project ID: {}".format(project_id))
print("Topic name: {}, Topic path: {}".format(topic_name, topic_path))
print("Subscription name: {}, Subscription path: {}".format(subscription_name, subscription_path))

project_path = 'projects/{}'.format(project_id)
print("Project path: {}".format(project_path))


"""Creates a new Pub/Sub subscription object listening to the
Container Analysis Occurrences topic."""
success = True
try:
    subscriber.create_subscription({"name": subscription_path, "topic": topic_path})
except AlreadyExists:
    # if subscription already exists, do nothing
    print("Already subscribed, doing nothing")
    pass
else:
    success = False


response = subscriber.pull(
    request={
        "subscription": subscription_path,
        "max_messages": 5,
    }
)

if len(response.received_messages) == 0:
    print("No messages, exiting")
    sys.exit(0)

for msg in response.received_messages:
    print("Received message:", msg.message.data)

ack_ids = [msg.ack_id for msg in response.received_messages]
subscriber.acknowledge(
    request={
        "subscription": subscription_path,
        "ack_ids": ack_ids,
    }
)
