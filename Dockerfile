FROM python:3.8

RUN pip install --upgrade google-cloud-pubsub

ENV GOOGLE_APPLICATION_CREDENTIALS="/secrets/service-account.json"
ENV PROJECT_ID="YOUR_GCP_PROJECT_ID"
ENV SUBSCRIPTION_NAME="poc-python-pubsub"
