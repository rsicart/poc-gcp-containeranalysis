import os

# Docs
# https://cloud.google.com/container-registry/docs/get-image-vulnerabilities#viewing_vulnerability_occurrences
#

def find_vulnerabilities_for_project(project_id):
    """"Retrieves all vulnerability occurrences associated with a resource."""
    # project_id = 'my-gcp-project'

    from google.cloud.devtools import containeranalysis_v1

    client = containeranalysis_v1.ContainerAnalysisClient()
    grafeas_client = client.get_grafeas_client()
    project_name = f"projects/{project_id}"

    filter_str = 'kind="VULNERABILITY"'
    return list(grafeas_client.list_occurrences(parent=project_name, filter=filter_str))


def get_image_from_url(url):
    url_splitted = url.split('/')
    image_with_tag = url_splitted[-1].split('@')
    return '{}@{}'.format(image_with_tag[0], image_with_tag[1][0:15])


def format_output(occurrence_list, severity_level=None):
    severity_levels = {
        'LOW': 2,
        'MEDIUM': 3,
        'HIGH': 4,
        'CRITICAL': 5,
    }
    formatted_occurrences = []
    for occurrence in occurrence_list:
        # ignore lower severity levels
        if severity_levels.get(severity_level, 0) > int(occurrence.vulnerability.effective_severity.value):
            continue
        occ = {}
        occ['resource_uri'] = occurrence.resource_uri
        occ['image_name'] = get_image_from_url(occurrence.resource_uri)
        occ['severity'] = occurrence.vulnerability.effective_severity
        occ['severity_name'] = occurrence.vulnerability.effective_severity.name
        occ['cve'] = occurrence.vulnerability.short_description
        occ['fix_available'] = occurrence.vulnerability.fix_available
        formatted_occurrences.append(occ)
    return formatted_occurrences


def group_by_image_and_severity(occurrence_list):
    grouped_occurrences = {}
    for occurrence in occurrence_list:
        key = occurrence.get('resource_uri', 'unknown')
        # init
        if key not in grouped_occurrences:
            grouped_occurrences[key] = {}
        try:
            grouped_occurrences[key][occurrence.get('severity_name')] += 1
        except KeyError:
            grouped_occurrences[key].update({occurrence.get('severity_name'): 1})
    return grouped_occurrences


def send_to_slack(webhook_url, title, occurrence_collection):
    from slack_sdk.webhook import WebhookClient
    # https://slack.dev/python-slack-sdk/webhook/index.html
    webhook = WebhookClient(webhook_url)
    blocks = [
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": title
            }
        },
    ]
    for image_uri, severities in occurrence_collection.items():
        severities_list = ['*{}*: {}'.format(k, v) for k, v in severities.items()]
        severities_text = ' | '.join(severities_list)
        blocks.append({
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": "*Image:* <{}|{}> | {}".format(
                    image_uri,
                    get_image_from_url(image_uri),
                    severities_text,
                )
            }
        })
    response = webhook.send(
        text=title,
        blocks=blocks
    )
    assert response.status_code == 200
    assert response.body == "ok"


if __name__ == '__main__':

    # ServiceAccount with role roles/containeranalysis.occurrences.viewer
    # do not forget to set GOOGLE_APPLICATION_CREDENTIALS
    project_id = os.environ.get('PROJECT_ID', None)
    # LOW, MEDIUM, HIGH, CRITICAL
    severity_level = os.environ.get('SEVERITY_LEVEL', None)
    vuln_list = find_vulnerabilities_for_project(project_id)

    formatted_vuln_list = format_output(vuln_list, severity_level='HIGH')
    print("Found {} results!".format(len(formatted_vuln_list)))
    for vuln in formatted_vuln_list:
        print(f"Vulnerability found: {vuln}")

    webhook_url = os.environ.get('SLACK_WEBHOOK_URL')
    title = '<{}|{}> Vulnerability report'.format(
        os.environ.get('CI_JOB_URL'),
        os.environ.get('CI_JOB_NAME'),
    )
    if webhook_url:
        send_to_slack(webhook_url, title, group_by_image_and_severity(formatted_vuln_list))

    grouped = group_by_image_and_severity(formatted_vuln_list)
    print(grouped)
