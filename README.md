# poc-gcp-containeranalysis

Get vulnerabilities form ContainerAnalysis API, and aggregates them by image.

Then they can be sent to SLACK, using an Incoming Webhook.

Can be used from a pipeline, for example using gitlab-ci.


## Requirements

Python 3.

GCP Project with Container Registry and Container Analysis active.


## Install

With pip (can be done in a virtualenv):

```
python3 -m venv venv
source venv/bin/activate
pip install wheel
pip install slack_sdk
pip install -r requirements.txt
```

## Gitlab-ci job example

```
image: python:3.8

stages:
  - dependencies
  - vulnerabilities


dependencies:
  stage: dependencies
  script:
    - python3 -m venv venv
    - source venv/bin/activate
    - pip install wheel
    - pip install slack_sdk
    - pip install -r requirements.txt
    - echo $GCP_SERVICEACCOUNT_KEY > credentials.json
  artifacts:
    paths:
      - venv
      - credentials.json
    expire_in: 1 hour

project vulnerabilities:
  stage: vulnerabilities
  before_script:
    - export GOOGLE_APPLICATION_CREDENTIALS="./credentials.json"
    - source venv/bin/activate
  script:
    - python get_vulns_api.py
```
